<?php

namespace Drupal\payment_gateway;

abstract class Controller extends PaymentMethodController {

  /**
   * @var Core
   */
  public $core = NULL;

  /**
   * @var boolean
   */
  public $cleanURL = false;

  /**
   * @var string
   */
  public $payment_method_configuration_form_elements_callback = 'payment_gateway_method_setting';

  /**
   * @var string
   */
  public $payment_configuration_form_elements_callback = 'payment_gateway_payment_setting';

  function __construct() {
    $core = PaymentGateway::getCore(get_class($this));
    $this->core = $core;
    $schema = $core->schemaFactory();
    $default = &$this->controller_data_defaults;

    foreach ($schema['fields'] as $index => $item) {
      $value = isset($item['default']) ? $item['default'] : '';
      $default[$index] = $value;
    }

    if (FALSE == empty($core->mode)) {
      $mode = reset($core->mode);
      $default['execute_mode'] = $mode['name'];
    }

    $default['text_format'] = filter_default_format();
    unset($default['pmid']);
  }

  // <editor-fold desc="OrderNumber">

  function encodeOrderNumber(Payment $payment) {
    $data = PaymentGateway::controllerData($payment->method);
    $code = substr($data['prefix_code'], 0, 1);
    $prefix = empty($code) ? ord('0') : ord($code);
    return sprintf('%c%03u%016X', $prefix, date('z'), $payment->pid);
  }

  function decodeOrderNumber($orderNumber) {
    $hex = substr($orderNumber, -16);
    return empty($hex) ? 0 : hexdec($hex);
  }

  // </editor-fold>
  // <editor-fold desc="Payment">

  function getPayment($orderNumber) {
    $payment = entity_load_single('payment', $this->decodeOrderNumber($orderNumber));
    return PaymentGateway::checkPayment($payment) ? $payment : NULL;
  }

  /**
   * @param string $action
   * @return null|Payment
   */
  function getPaymentByAction($action) {
    $list = &$this->core->action;

    if (empty($list[$action])) {
      return NULL;
    }

    $index = $list[$action]['callbackOrderNumberIndex'];
    return empty($_REQUEST[$index]) ? NULL : $this->getPayment($_REQUEST[$index]);
  }

  function getPaymentList($action) {
    $core = $this->core;
    $setting = &$core->action[$action];
    $query = db_select('payment', 'payment');
    $query->orderRandom()->fields('payment', array('pid'));
    $query->join('payment_method', 'payment_method', 'payment.pmid = payment_method.pmid');
    $query->join('payment_status_item', 'payment_status_item', 'payment.psiid_last = payment_status_item.psiid');
    $query->condition('payment_method.controller_class_name', $core->controller);
    $query->condition('payment_status_item.status', $setting['statusList'], 'IN');
    $query->range(0, $setting['batchNumber']);
    return entity_load('payment', $query->execute()->fetchCol());
  }

  // </editor-fold>
  // <editor-fold desc="Check">
  function checkAmount(Payment $payment) {
    $core = $this->core;
    $totalPrice = PaymentGateway::total($payment);

    if (0 >= $totalPrice['rate']) {
      $core->setError($totalPrice['message']);
      return FALSE;
    }

    $index = 'value';
    $amount = $totalPrice[$index];
    $data = PaymentGateway::controllerData($payment->method);
    $minimum = floatval($data['minimum_amount']);
    $maximum = (0 == $data['maximum_amount']) ? PHP_INT_MAX : floatval($data['maximum_amount']);

    if (($minimum < $amount) && ($maximum > $amount)) {
      return TRUE;
    }

    PaymentGateway::setFormatCurrencyOption($totalPrice, $data);
    $minimumPrice = array($index => $minimum) + $totalPrice;
    $maximumPrice = array($index => $maximum) + $totalPrice;

    $core->setError(t('Total (@total) is not between @minimum and @maximum.', array(
      '@total' => PaymentGateway::formatCurrency($totalPrice),
      '@minimum' => PaymentGateway::formatCurrency($minimumPrice),
      '@maximum' => PaymentGateway::formatCurrency($maximumPrice),
    )));

    return FALSE;
  }

  function checkSetting(Payment $payment) {
    global $conf;
    $check = TRUE;
    $core = $this->core;

    if ($this->cleanURL && (empty($conf['clean_url']) || (1 != $conf['clean_url']))) {
      $core->setError(t('Clean URL is not enabled.'));
      $check = FALSE;
    }

    $data = PaymentGateway::controllerData($payment->method);

    if (empty($data['merchant_code'])) {
      $core->setError(t('Merchant Code is not set.'));
      $check = FALSE;
    }

    if (empty($data['merchant_key'])) {
      $core->setError(t('Merchant Key is not set.'));
      $check = FALSE;
    }

    foreach ($core->mode as $mode) {
      foreach ($core->action as $action) {
        if ($action['offline']) {
          continue;
        }

        $index = PaymentGateway::invokeUrlKey($mode['name'], $action['name']);

        if (FALSE == valid_url($data[$index])) {
          $core->setError(t('@mode @action URL is not valid.', array('@mode' => $mode['title'], '@action' => $action['title'])));
          $check = FALSE;
        }
      }
    }

    return $check;
  }

  // </editor-fold>
  // <editor-fold desc="Currency">
  // </editor-fold>

  function execute(Payment $payment) {
    $action = 'payment';
    $do = PaymentGateway:: getAction($action, $payment);

    if (is_null($do)) {
      $class = PaymentGateway::className($payment->method->controller->name, $action);
      $this->core->setError(t('Load Action (@class) failure.', array('@class' => $class)));
      return;
    }

    return $do->process();
  }

  function batch($action) {
    $data = array();
    $list = $this->getPaymentList($action);

    foreach ($list as $index => $payment) {
      $do = PaymentGateway:: getAction($action, $payment);

      if (is_null($do)) {
        $class = PaymentGateway::className($payment->method->controller->name, $action);
        $data[$index] = array(t('Load Action (@class) failure.', array('@class' => $class)));
        continue;
      }

      $core = $do->core;

      if ($core->action[$action]['autoRedirect']) {
        continue;
      }

      $core->error = array();
      $do->batch = TRUE;
      $do->process();
      $data[$index] = $core->error;
      $core->error = array();
    }

    return $data;
  }

  function callback($action) {
    $form = array('message' => array('#type' => 'markup'));
    $core = $this->core;
    $class = PaymentGateway:: className($this->name, $action);
    $setting = &$core->action;

    if (empty($setting[$action])) {
      $form['#markup'] = check_markup(t('Action (@class) setting is not exists.', array('@class' => $class)));

      return $form;
    }

    if ($setting[$action]['callbackBatch']) {
      return $this->renderBatch($this->batch($action));
    }

    $payment = $this->getPaymentByAction($action);

    if (is_null($payment) || ($payment->method->controller->name != $this->name)) {
      $form['#markup'] = check_markup(t('Payment is not valid.'));

      return $form;
    }

    $do = PaymentGateway:: getAction($action, $payment);

    if (is_null($do)) {
      $form['#markup'] = check_markup(t('Load action (@class) failure.', array('@class' => $class)));

      return $form;
    }

    return $do->callback();
  }

  function renderBatch(array &$data) {
    $form = array('batch' => array('#type' => 'markup'));
    $list = array();

    foreach ($data as $key => $value) {
      $status = empty($value) ? t('success') : t('failure');
      $list[] = t('Payment @key execute @status.', array('@key' => $key, '@status' => $status));
    }

    $form['#markup'] = theme('item_list', array('items' => $list));
    return $form;
  }

}

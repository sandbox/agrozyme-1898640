<?php

namespace Drupal\payment_gateway;

abstract class Action {

  /**
   * @var string
   */
  public $action = '';

  /**
   * @var string lowercase string
   */
  public $name = '';

  /**
   * @var Core
   */
  public $core = NULL;

  /**
   * @var Payment
   */
  public $payment = NULL;

  /**
   * @var boolean
   */
  public $batch = FALSE;

  /**
   * @var boolean
   */
  public $doCheckStatus = TRUE;

  /**
   * @var array
   */
  public $checkBeforeStatusList = array();

  /**
   * @var array
   */
  public $checkAfterStatusList = array();

  /**
   * @var string
   */
  public $beforeStatus = '';

  /**
   * @var string
   */
  public $afterStatus = '';

  /**
   * @var array
   */
  public $data = array();

  function __construct(Payment $payment) {
    $class = get_class($this);
    $this->name = $class;
    $names = PaymentGateway::explodeCamel($class);
    $this->action = drupal_strtolower(end($names));
    $this->core = PaymentGateway::getCore($class);
    $this->payment = $payment;
    $this->afterStatus = PaymentGateway::getStatus($payment);
    $this->data = PaymentGateway::controllerData($payment->method);
  }

  // <editor-fold desc="Invoke">
  function invokeCheck() {
    $core = $this->core;

    if ($core->hasError()) {
      return FALSE;
    }

    $payment = $this->payment;
    $controller = $payment->method->controller;

    if (FALSE == $controller->checkSetting($payment)) {
      return FALSE;
    }

    if (FALSE == $controller->checkAmount($payment)) {
      return FALSE;
    }

    if ($this->doCheckStatus && (FALSE == $this->checkStatus($this->checkBeforeStatusList))) {
      return FALSE;
    }

    return TRUE;
  }

  function invokeUrl() {
    $data = &$this->data;
    $index = PaymentGateway::invokeUrlKey($data['execute_mode'], $this->action);
    return (empty($data[$index])) ? '' : $data [$index];
  }

  function invokeKey(array $data) {
    return '';
  }

  function invokeData() {
    return array();
  }

  function invokeRequestData(array &$data, $isPost = TRUE, $isForm = TRUE) {
    $method = ($isPost) ? 'POST' : 'GET';

    if ($isForm) {
      return array(
        'method' => $method,
        'headers' => array('Content-Type' => 'application/x-www-form-urlencoded; charset=UTF-8'),
        'data' => http_build_query($data),
      );
    } else {
      return array(
        'method' => $method,
        'data' => $data,
      );
    }
  }

  // </editor-fold>
  // <editor-fold desc="Return">

  function returnOrderNumber() {
    return '';
  }

  function returnKey(array $data) {
    return '';
  }

  function returnData() {
    $index = $this->name;
    if ($this->core->action['payment']['autoRedirect']) {
      return $_REQUEST;
    } else {
      $data = &$this->payment->context_data[$index]['return']['raw']['data'];
      return $data;
    }
  }

  function returnCheck() {
    $core = $this->core;
    $index = $this->name;
    $data = (array) $core->payment->context_data[$index]['return']['data'];
    $keyList = (array) $core->action[$index]['returnFieldList'];
    dpm($index);
    dpm($core->payment);
    dpm($core->action);

    if (FALSE == PaymentGateway::existAllKey($data, $keyList)) {
      $core->setError(t('Transaction data is not valid.'));
      return FALSE;
    }

    if ($this->doCheckStatus && (FALSE == $this->checkStatus($this->checkAfterStatusList))) {
      return FALSE;
    }

    return TRUE;
  }

  function returnUrl() {
    return url(implode('/', PaymentGateway:: formatName($this->name, '', FALSE)), array('absolute' => true));
  }

  // </editor-fold>

  function updateAmount() {
    $payment = $this->payment;
    $amount = $payment->totalAmount(true);
    $data = PaymentGateway::controllerData($payment->method);
    $from = $payment->currency_code;
    $to = $data['payment_currency'];
    $key = 'value';
    $decimal = $data['payment_decimal'];
    $list = &$payment->line_items;

    foreach ($list as $index => $item) {
      $convert = PaymentGateway::convertCurrency($from, $to, $item->amount);
      $list[$index]->amount = PaymentGateway::numberScale($convert[$key], $decimal);
    }

    $convert = PaymentGateway::convertCurrency($from, $to, $amount);
    $total = PaymentGateway::numberScale($convert[$key], $decimal);

    $price = array();
    $keep = array('code', 'rate', 'message', 'timestamp', 'from_currency', 'from_value', 'value');

    foreach ($keep as $index) {
      $price[$index] = $convert[$index];
    }

    $payment->currency_code = $to;
    $payment->context_data['price'] = $price;
    $payment->context_data['total'] = $total;
  }

  function setStatus($status) {
    if (FALSE == empty($status)) {
      $this->beforeStatus = $status;
      $this->afterStatus = $status;
    }
  }

  function checkStatus(array $list) {
    if ((FALSE != empty($list)) && (in_array(PaymentGateway::getStatus($this->payment), $list))) {
      $this->core->setError(t('Payment Order status is not valid.'));
      return FALSE;
    }

    return TRUE;
  }

  function process() {
    return $this->execute();
  }

  function before() {
    $data = &$this->payment->context_data[$this->name]['invoke'];
    $data['url'] = $this->invokeUrl();
    $data['data'] = $this->invokeData();
  }

  function execute() {
    $this->invokeCheck();
    $core = &$this->core;

    if ($core->hasError()) {
      return $this->renderFailure();
    }

    $this->before();
    $this->setStatus($this->beforeStatus);
    $this->paymentSave($this->beforeStatus);
    $index = $this->name;
    $data = &$this->payment->context_data[$index];
    $invoke = &$data['invoke'];

    if ($core->action[$index]['autoRedirect']) {
      PaymentGateway:: autoSubmitPage($invoke['url'], $invoke['data']);
    } else {
      $data['return']['raw'] = drupal_http_request($invoke['url'], $invoke['data']);
      return $this->after();
    }
  }

  function after() {
    $core = $this->core;
    $return = &$this->payment->context_data[$this->name]['return'];
    $return['data'] = $this->returnData();
    $this->returnCheck();
    $return['error'] = $core->error;
    $this->paymentSave($this->afterStatus);

    if ($this->batch) {
      return array();
    }

    if ($core->hasError()) {
      return $this->renderFailure();
    } else {
      return $this->renderSuccess();
    }
  }

  function callback() {
    if (FALSE == $this->core->hasError()) {
      return $this->after();
    }

    return $this->batch ? array() : $this->renderFailure();
  }

  function paymentSave($status) {
    $payment = $this->payment;

    if (FALSE == empty($status)) {
      $payment->setStatus(new PaymentStatusItem($status));
    }

    entity_save('payment', $payment);
    $this->debug();
  }

  function debug() {
    $payment = $this->payment;

    $message = t('Payment Order @id @action Status: @status.', array(
      '@id' => $payment->pid,
      '@action' => $this->core->action[$this->name]['title'],
      '@status' => payment_status_info($payment->status)->title,
    ));

    payment_debug($message);
  }

  // <editor-fold desc="Render">
  function render(array &$data) {
    if ($this->core->action['payment']['autoRedirect']) {
      return $data;
    } else {
      PaymentGateway::renderExit($data);
    }
  }

  function successView() {
    return entity_view('payment', array($this->payment));
  }

  function failureView() {
    $core = $this->core;
    $title = t('Execute @title failure.', array('@title' => $core->action[$this->name]['title']));
    $form = array('failure' => array('#type' => 'markup'));
    $form['#markup'] = theme('item_list', array('items' => $core->error, 'title' => $title));
    return $form;
  }

  function renderSuccess() {
    $data = $this->successView();
    return $this->render($data);
  }

  function renderFailure() {
    $data = $this->failureView();
    return $this->render($data);
  }

  // </editor-fold>
}

<?php

namespace Drupal\payment_gateway;

abstract class Query extends Action {

  public $batch = TRUE;

  function process() {
    return $this->execute();
  }

}

<?php

namespace Drupal\payment_gateway;

class PaymentGateway {

  // <editor-fold desc="Name">

  const NAME_KEEP = 4;

  /**
   * @param string $text
   * @param string $type
   * @return string
   */
  static function className($text, $type = '') {
    return implode('', self::formatName($text, $type, TRUE));
  }

  /**
   * @param string $text
   * @return string
   */
  static function tableName($text) {
    return implode('_', self::formatName($text, '', FALSE));
  }

  /**
   * @param string $mode
   * @param string $action
   * @return string
   */
  static function invokeUrlKey($mode, $action) {
    return drupal_strtolower($mode) . '_' . drupal_strtolower($action) . '_url';
  }

  /**
   * @param string $text
   * @param string $action
   * @return string
   */
  static function queueClassKey($text, $action) {
    return 'queue_class_' . self::tableName($text) . '_' . drupal_strtolower($action);
  }

  /**
   * @param string $text
   * @param string $type
   * @param boolean $toCamel
   * @return string
   */
  static function formatName($text, $type = '', $toCamel = TRUE) {
    $list = self::explodeName($text);
    $max = (empty($type)) ? self::NAME_KEEP : self::NAME_KEEP + 1;
    $data = array();

    for ($index = 0; $index < $max; $index++) {
      if ($index < self::NAME_KEEP) {
        $item = empty($list[$index]) ? '' : $list[$index];
      } else {
        $item = $type;
      }

      if ('' == $item) {
        $data[] = '';
        continue;
      }

      $item = drupal_strtolower($item);
      $data[] = ($toCamel) ? drupal_ucfirst($item) : $item;
    }

    return $data;
  }

  /**
   * @param string $text
   * @return array
   */
  static function explodeName($text) {
    $data = &drupal_static(__METHOD__);

    if (FALSE == empty($data[$text])) {
      return $data[$text];
    }

    $data[$text] = (FALSE === strpos($text, '_')) ? self::explodeCamel($text) : explode('_', $text);
    return $data[$text];
  }

  /**
   * @param string $text
   * @return array
   */
  static function explodeCamel($text) {
    $cache = &drupal_static(__METHOD__);

    if (isset($cache[$text])) {
      return $cache[$text];
    }

    $length = drupal_strlen($text);
    $char = array();
    $data = &$cache[$text];

    for ($index = 0; $index < $length; $index++) {
      $item = drupal_substr($text, $index, 1);
      $last = end($char);
      $itemCase = self::isBeginUpperCase($item);
      $lastCase = self::isBeginUpperCase($last);

      if ($itemCase && (FALSE == $lastCase) && (0 < count($char))) {
        $data[] = implode('', $char);
        $char = array();
      }

      if ($lastCase && (FALSE == $itemCase) && (1 < count($char))) {
        $push = array_pop($char);
        $data[] = implode('', $char);
        $char = array($push);
      }


      $char[] = $item;
    }

    if (FALSE == empty($char)) {
      $data[] = implode('', $char);
    }

    return $data;
  }

  /**
   * @param string $text
   * @return boolean
   */
  static function isBeginUpperCase($text) {
    $data = &drupal_static(__METHOD__);

    if (empty($data)) {
      $data = array(ord('A') - 1, ord('Z') + 1);
    }

    $item = ord($text);
    return ($item > reset($data)) && ($item < end($data));
  }

  // </editor-fold>
  // <editor-fold desc="Object">

  /**
   * @param Payment $object
   * @return boolean
   */
  static function checkPayment($object) {
    return self::isObject($object, 'Payment') && self::checkMethod($object->method);
  }

  /**
   * @param PaymentMethod $object
   * @return boolean
   */
  static function checkMethod($object) {
    return self::isObject($object, 'PaymentMethod') && self::checkController($object->controller);
  }

  /**
   * @param Controller $object
   * @return boolean
   */
  static function checkController($object) {
    return self::isObject($object, 'PaymentGatewayController');
  }

  /**
   * @param mixed $object
   * @return boolean
   */
  static function isCore($object) {
    return self::isObject($object, 'PaymentGatewayCore');
  }

  /**
   * @param Action $object
   * @return boolean
   */
  static function isAction($object) {
    return self::isObject($object, 'PaymentGatewayAction');
  }

  /**
   * @param mixed $object
   * @param mixed $class
   * @return boolean
   */
  static function isObject($object, $class) {
    return is_object($object) && ($object instanceof $class);
  }

  /**
   * @param string $text
   * @return null|Core
   */
  static function getCore($text) {
    try {
      $data = &drupal_static(__METHOD__);
      $class = self::className($text, 'Core');

      if (FALSE == class_exists($class)) {
        throw new Exception(t('Class (@class) is not exists.', array('@class' => $class)));
      }

      if (FALSE == isset($data[$class])) {
        $data[$class] = new $class();
      }

      if (FALSE == PaymentGateway::isCore($data[$class])) {
        throw new Exception(t('Core (@class) is not vaild.', array('@class' => $class)));
      }

      return $data[$class];
    } catch (Exception $error) {
      payment_debug($error->getMessage());
      return NULL;
    }
  }

  /**
   * @param string $action
   * @param Payment $payment
   * @return null|Action
   */
  static function getAction($action, Payment $payment) {
    if (FALSE == self::checkPayment($payment)) {
      return NULL;
    }

    try {
      $class = self::className($payment->method->controller->name, $action);
      $item = new $class($payment);

      if (FALSE == PaymentGateway::isAction($item)) {
        throw new Exception(t('Action (@class) is not vaild.', array('@class' => $class)));
      }

      return $item;
    } catch (Exception $error) {
      payment_debug($error->getMessage());
      return NULL;
    }
  }

  /**
   * @param Payment $payment
   * @return string
   */
  static function getStatus(Payment $payment) {
    $status = $payment->getStatus();
    return self::isObject($status, 'PaymentStatusItem') ? $status->status : PAYMENT_STATUS_UNKNOWN;
  }

  /**
   * @param string $controller
   * @param array $statusList
   * @param int $limit
   * @return array
   */
  static function getPaymentList($controller, array &$statusList = array(), $limit = 0) {
    $query = db_select('payment', 'payment');
    $query->orderRandom()->fields('payment', array('pid'));
    $query->join('payment_method', 'payment_method', 'payment.pmid = payment_method.pmid');
    $query->join('payment_status_item', 'payment_status_item', 'payment.psiid_last = payment_status_item.psiid');
    $query->condition('payment_method.controller_class_name', $controller);

    if (FALSE == empty($statusList)) {
      $query->condition('payment_status_item.status', $statusList, 'IN');
    }

    if (0 < $limit) {
      $query->range(0, $limit);
    }

    return entity_load('payment', $query->execute()->fetchCol());
  }

  // </editor-fold>
  // <editor-fold desc="Currency">

  const CURRENCY_BCMATH_SCALE = 10;
  const FORMAT_CURRENCY_EXPIRED = 0;
  const FORMAT_CURRENCY_ACTIVE = 1;
  const FORMAT_CURRENCY_LEFT = 0;
  const FORMAT_CURRENCY_RIGHT = 1;

  /**
   * @param float $number
   * @param int $scale
   * @return float
   */
  static function numberScale($number, $scale) {
    if (0 == $scale) {
      return ceil($number);
    }

    $base = pow(10, $scale);

    if (0 > $scale) {
      return ceil($number * $base) / $base;
    } else {
      return ceil($number / $base) * $base;
    }
  }

  /**
   * @param array $data
   * @return string
   */
  static function formatCurrency(array $data) {
    $data += self::currencyDefault();

    if (0 >= $data['rate']) {
      return '<span class="format-currency format-currency-error">' . t('An error occurred: @message', array('@message' => $data['message'])) . '</span>';
    } else {
      return '<span class="format-currency format-currency-' . $data['code'] . '">' . self::formatCurrencyParse($data) . '</span>';
    }
  }

  /**
   * @param array $data
   * @return string
   */
  static function formatCurrencyParse(array $data) {
    $data += self::currencyDefault();
    $price = number_format($data['value'], $data['decimal_places'], $data['decimal_separator'], $data['thousands_separator']);
    $symbol = empty($data['symbol']) ? $data['entity'] : $data['symbol'];
    $list = ($data['display_currency_code']) ? array($data['code'] . ' ') : array();

    switch ($data['symbol_position']) {
      case self::FORMAT_CURRENCY_RIGHT:
        $list[] = $price;
        $list[] = $symbol;
        break;

      case self::FORMAT_CURRENCY_LEFT:
      default:
        $list[] = $symbol;
        $list[] = $price;
        break;
    }

    return filter_xss(implode('', $list));
  }

  /**
   * @param array $data
   * @param array $option
   * @return null
   */
  static function setFormatCurrencyOption(array &$data, array &$option) {
    $data['base'] = pow(10, $option['display_decimal']);
    $data['decimal_places'] = $option['display_decimal'];
    $data['display_currency_code'] = $option['display_currency_code'];
  }

  /**
   * @param Payment $payment
   * @return array
   */
  static function total(Payment $payment) {
    $method = $payment->method;
    $controller = $method->controller;
    $amount = $payment->totalAmount(true);
    $data = self::controllerData($method);
    $from = $payment->currency_code;

    if (self::checkController($controller)) {
      return self::convertCurrency($from, $data['payment_currency'], $amount);
    }

    $price = self::currencyDefault();
    $price['message'] = t('Controller (@class) is not valid.', array('@class' => $controller->name));
    $price['from_currency'] = $from;
    $price['from_value'] = $amount;
    return $price;
  }

  /**
   * @return array
   */
  static function currencyDefault() {
    return array(
      'code' => 'XXX',
      'num' => 999,
      'name' => t('No currency (XXX)'),
      'base' => 1,
      'links' => array(),
      'symbol' => '',
      'entity' => '',
      'coinage_symbol' => '',
      'coinage_entity' => '',
      'decimal_places' => 0,
      'decimal_separator' => '.',
      'thousands_separator' => ',',
      'symbol_position' => PaymentGateway::FORMAT_CURRENCY_LEFT,
      'coinage_position' => PaymentGateway::FORMAT_CURRENCY_RIGHT,
      'status' => PaymentGateway::FORMAT_CURRENCY_ACTIVE,
      'rate' => 0.0,
      'message' => '',
      'timestamp' => REQUEST_TIME,
      'from_currency' => 'XXX',
      'from_value' => 0.0,
      'value' => 0.0,
      'display_currency_code' => FALSE,
    );
  }

  /**
   * @param string $code
   * @return array
   */
  static function currency($code) {
    $data = Currency::getList();
    return isset($data[$code]) ? $data[$code] : array('code' => $code) + self::currencyDefault();
  }

  /**
   * @param string $fromCurrency
   * @param string $toCurrency
   * @return array
   */
  static function exchangeRate($fromCurrency, $toCurrency) {
    $version = self::explodeVersion('module', 'currency');
    $data = self::currency($toCurrency);
    $data['timestamp'] = REQUEST_TIME;
    $data['from_currency'] = $fromCurrency;

//    if (1 == floatval($version)) {
//      $convert = currency_api_convert($fromCurrency, $toCurrency, 1);
//
//      if (FALSE === $convert) {
//        $data['message'] = t('failure');
//      } else {
//        $convert += $data;
//        $data['rate'] = floatval($convert['rate']);
//        $data['message'] = $convert['message'];
//      }
//    } else {
    try {
      $rate = (float) CurrencyExchanger::load($fromCurrency, $toCurrency);
      $data['rate'] = $rate;
      $data['message'] = t('success');
    } catch (Exception $exception) {
      $data['message'] = $exception->getMessage();
    }
//    }

    return $data;
  }

  /**
   * @param string $fromCurrency
   * @param string $toCurrency
   * @param float $amount
   * @return array
   */
  static function convertCurrency($fromCurrency, $toCurrency, $amount) {
    $data = self::exchangeRate($fromCurrency, $toCurrency);
    $data['from_value'] = $amount;
    $data['value'] = $amount * $data['rate'];
    return $data;
  }

  // </editor-fold>
  /**
   * @param string $text
   * @return float
   */
  static function formatVersion($text) {
    return floatval(strtr(drupal_strtolower($text), 'x', '0'));
  }

  /**
   * @param string $type
   * @param string $name
   * @return array
   */
  static function explodeVersion($type, $name) {
    $data = &drupal_static(__METHOD__);

    if (isset($data[$type][$name])) {
      return $data[$type][$name];
    }

    $info = system_get_info('module', $name);

    if (empty($info)) {
      return array(
        'core' => VERSION,
        'version' => -1,
        'dev' => '',
      );
    }

    $list = explode('-', $info['version']);

    $data[$type][$name] = array(
      'core' => self::formatVersion($list[0]),
      'version' => self::formatVersion($list[1]),
      'dev' => isset($list[2]) ? $list[2] : '',
    );

    return $data[$type][$name];
  }

  /**
   * @param string $text
   * @return array
   */
  static function schema($text) {
    $core = self::getCore($text);

    if (is_null($core)) {
      return array();
    }

    $table = $core->table;
    $schema = $core->schemaFactory();
    return array($table => $schema);
  }

  /**
   * @param Paymethod $method
   * @return array
   */
  static function controllerData(PaymentMethod $method) {
    return $method->controller_data + $method->controller->controller_data_defaults;
  }

  /**
   * @param array $form
   */
  static function renderExit(array &$form) {
    $page = element_info('page');
    $page['content']['system_main'] = array('form' => array(
        '#type' => 'markup',
        '#markup' => drupal_render($form),
      ),);
    print drupal_render($page);
    drupal_exit();
  }

  /**
   * @param string $url
   * @param array $data
   */
  static function autoSubmitPage($url, array &$data) {
    $form = array(
      '#type' => 'form',
      '#action' => $url,
      '#method' => 'POST',
    );

    foreach ($data as $key => $value) {
      $form[$key] = array(
        '#type' => 'hidden',
        '#name' => $key,
        '#value' => $value,
      );
    }

    $form['#attributes']['class'][] = 'payment-gateway';
    $form['#attached']['js'][] = drupal_get_path('module', 'payment_gateway') . '/payment_gateway_auto_submit.js';
    self::renderExit($form);
  }

  /**
   * @param array $data
   * @param array $keyList
   * @return boolean
   */
  static function existAllKey(array &$data, array &$keyList) {
    foreach ($keyList as $key) {
      if (FALSE == (isset($data[$key]) || array_key_exists($key, $data))) {
        return FALSE;
      }
    }

    return TRUE;
  }

  /**
   * @param array $element
   * @param array $form_state
   * @return array
   */
  static function mergeFormData(array $element, array &$form_state) {
    if (isset($form_state['values'])) {
      $value = &$form_state['values'];
    } else {
      $value = array();
    }

    if (isset($element['#parents'])) {
      $parent = &$element['#parents'];
    } else {
      $parent = array();
    }

    return drupal_array_get_nested_value($value, $parent);
  }

  /**
   * @param string $message
   * @param string $type
   * @param bool $repeat
   * @return array|null
   */
  static function setMessage($message = NULL, $type = 'status', $repeat = TRUE) {
    return drupal_set_message(check_markup($message), $type, $repeat);
  }

}

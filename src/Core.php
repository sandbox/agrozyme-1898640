<?php

namespace Drupal\payment_gateway;

abstract class Core {

  /**
   * @var string
   */
  public $name = '';

  /**
   * @var string
   */
  public $table = '';

  /**
   * @var string
   */
  public $controller = '';

  /**
   * @var array
   */
  public $schema = array();

  /**
   * @var array
   */
  public $mode = array();

  /**
   * @var array
   */
  public $action = array();

  /**
   * @var array
   */
  public $error = array();

  function __construct() {
    $class = get_class($this);
    $this->name = $class;
    $this->table = PaymentGateway::tableName($class);
    $this->controller = PaymentGateway::className($class, 'Controller');
    $this->mode = $this->initMode();
    $this->action = $this->initAction();
  }

  function initMode() {
    $data = array();

    $data['development'] = array(
      'name' => 'development',
      'title' => t('Development'),
    );

    $data['production'] = array(
      'name' => 'production',
      'title' => t('Production'),
    );

    return $data;
  }

  /**
   * @return array
   */
  function initAction() {
    $data = array();

    $data['payment'] = array(
      'name' => 'payment',
      'title' => t('Payment'),
      'offline' => FALSE,
      'autoRedirect' => TRUE,
      'callbackBatch' => FALSE,
      'allowCallback' => TRUE,
      'allowInvokeAgo' => 0,
      'allowInvokeStatusList' => array(),
      'allowReturnStatusList' => array(),
      'returnCodeKey' => '',
      'returnOrderNumberKey' => '',
      'returnSuccessKeyList' => array(),
      'returnFailureKeyList' => array(),
      'returnStatusTransferList' => array(),
    );

    $data['query'] = array(
      'name' => 'query',
      'title' => t('Query'),
      'offline' => FALSE,
      'autoRedirect' => FALSE,
      'callbackBatch' => TRUE,
      'allowCallback' => FALSE,
      'allowInvokeAgo' => 0,
      'allowInvokeStatusList' => array(),
      'allowReturnStatusList' => array(),
      'returnCodeKey' => '',
      'returnOrderNumberKey' => '',
      'returnSuccessKeyList' => array(),
      'returnFailureKeyList' => array(),
      'returnStatusTransferList' => array(),
    );

    return $data;
  }

  // <editor-fold desc="Schema">

  /**
   * @return array
   */
  function schemaFactory() {
    if (empty($this->schema)) {
      $this->schema = $this->schemaBody();
    }

    return $this->schema;
  }

  /**
   * @return array
   */
  function schemaBody() {
    $data = array(
      'fields' => array(),
      'primary key' => array('pmid'),
      'unique keys' => array(
        'pmid' => array('pmid'),
      ),
    );

    $field = &$data['fields'];

    $field['pmid'] = array(
      'type' => 'int',
      'unsigned' => TRUE,
      'not null' => TRUE,
      'default' => 0,
    );

    $field['message'] = array(
      'type' => 'text',
      'size' => 'big',
      'not null' => TRUE,
    );

    $field['text_format'] = array(
      'type' => 'varchar',
      'length' => 255,
      'not null' => TRUE,
      'default' => '',
    );

    $field['batch_number'] = array(
      'type' => 'int',
      'unsigned' => TRUE,
      'not null' => TRUE,
      'default' => 0,
    );

    $field['prefix_code'] = array(
      'type' => 'varchar',
      'length' => 1,
      'not null' => TRUE,
      'default' => '0',
    );

    $field['execute_mode'] = array(
      'type' => 'varchar',
      'length' => 128,
      'not null' => TRUE,
      'default' => '',
    );

    $field['payment_currency'] = array(
      'type' => 'varchar',
      'length' => 3,
      'not null' => TRUE,
      'default' => '',
    );

    $field['exchange_rate'] = array(
      'type' => 'float',
      'unsigned' => TRUE,
      'not null' => TRUE,
      'default' => 0,
    );

    $field['minimum_amount'] = array(
      'type' => 'float',
      'unsigned' => TRUE,
      'not null' => TRUE,
      'default' => 0,
    );

    $field['maximum_amount'] = array(
      'type' => 'float',
      'unsigned' => TRUE,
      'not null' => TRUE,
      'default' => 0,
    );

    $field['payment_decimal'] = array(
      'type' => 'int',
      'size' => 'tiny',
      'not null' => TRUE,
      'default' => 0,
    );

    $field['display_decimal'] = array(
      'type' => 'int',
      'size' => 'tiny',
      'not null' => TRUE,
      'default' => 0,
    );

    $field['display_currency_code'] = array(
      'type' => 'int',
      'size' => 'tiny',
      'unsigned' => TRUE,
      'not null' => TRUE,
      'default' => 0,
    );

    $field['merchant_code'] = array(
      'type' => 'varchar',
      'length' => 128,
      'not null' => TRUE,
      'default' => '',
    );

    $field['merchant_key'] = array(
      'type' => 'varchar',
      'length' => 128,
      'not null' => TRUE,
      'default' => '',
    );

    $this->modeActionLoop($field, NULL, array($this, 'schemaActionCallback'));
    return $data;
  }

  /**
   * @param array $data
   * @param array $mode
   * @param array $action
   */
  function schemaActionCallback(array &$data, array $mode, array $action) {
    if ($action['offline']) {
      return;
    }

    $index = PaymentGateway::invokeUrlKey($mode['name'], $action['name']);

    $data[$index] = array(
      'type' => 'varchar',
      'length' => 2048,
      'not null' => TRUE,
      'default' => '',
    );
  }

  // </editor-fold>
  // <editor-fold desc="Error">

  function setError($text) {
    $this->error[] = $text;

    if (variable_get('payment_debug', TRUE)) {
      watchdog('Payment', $text);
    }
  }

  function showError() {
    foreach ($this->error as $text) {
      drupal_set_message($text, 'error');
    }
  }

  function hasError() {
    return !empty($this->error);
  }

  // </editor-fold>
  // <editor-fold desc="Entity">

  /**
   * @param PaymentMethod $method
   * @return boolean
   */
  function sameControllerName($method) {
    return PaymentGateway::checkMethod($method) && ($this->controller == $method->controller->name);
  }

  function load(array $data, $type) {
    if ('payment_method' != $type) {
      return;
    }

    $list = array();

    foreach ($data as $method) {
      if ($this->sameControllerName($method)) {
        $list[] = $method->pmid;
      }
    }

    if (empty($list)) {
      return;
    }

    $table = $this->table;
    $result = db_select($table)->fields($table)->condition('pmid', $list)->execute();

    while ($item = $result->fetchAssoc()) {
      if (FALSE == isset($item['pmid'])) {
        continue;
      }

      $index = $item['pmid'];
      unset($item['pmid']);
      $data[$index]->controller_data = $item;
    }
  }

  function save(PaymentMethod $method, $isUpdate) {
    if (FALSE == $this->sameControllerName($method)) {
      return;
    }

    $pmid = $method->pmid;
    $data = array('pmid' => $pmid) + PaymentGateway::controllerData($method);
    $table = $this->table;
    $query = $isUpdate ? db_update($table)->condition('pmid', $pmid) : db_insert($table);
    $query->fields($data)->execute();
  }

  function delete(PaymentMethod $method) {
    if ($this->sameControllerName($method)) {
      db_delete($this->table)->condition('pmid', $method->pmid)->execute();
    }
  }

  // </editor-fold>
  // <editor-fold desc="Option">

  function batchNumberOption() {
    $data = range(10, 100, 10);
    return array_combine($data, $data);
  }

  function prefixCodeOption() {
    $data = array();
    $number = range(ord('0'), ord('9'));
    $alpha = range(ord('A'), ord('Z'));

    foreach ($number as $item) {
      $data[] = chr($item);
    }

    foreach ($alpha as $item) {
      $data[] = chr($item);
    }

    return array_combine($data, $data);
  }

  function executeModeOption() {
    $data = array();

    foreach ($this->mode as $key => $item) {
      $data[$key] = $item['title'];
    }

    return $data;
  }

  function paymentCurrencyOption() {
    $data = array();
    $list = Currency::getList();

    foreach ($list as $index => $item) {
      $data[$index] = $index . ' - ' . $item['name'];
    }

    return $data;
  }

  function displayCurrencyCodeOption() {
    return array(0 => t('No'), 1 => t('Yes'));
  }

  // </editor-fold>
  // <editor-fold desc="Method Setting">

  function methodSetting(array $element, array &$form_state) {
    $form = array();
    $index = 'payment_method';

//		if ((FALSE == isset($form_state[$index])) || (FALSE == PaymentGateway::checkMethod($form_state[$index]))) {
//			drupal_set_message(check_markup(t('Method is not valid.')), 'error');
//			return $form;
//		}
//		dpm($form_state);
    $schema = $this->schemaFactory();

    $data = array(
      'default' => PaymentGateway::controllerData($form_state[$index]),
      'field' => $schema['fields'],
      'form' => &$form,
    );

    $form['basic'] = $this->methodSettingBasicFieldset($data);
    $form['currency'] = $this->methodSettingCurrencyFieldset($data);
    $this->modeActionLoop($data, array($this, 'methodSettingModeCallback'), array($this, 'methodSettingActionCallback'));
    return $form;
  }

  function methodSettingBasicFieldset(array $data) {
    $default = $data['default'];
    $field = $data['field'];
    $list = array('#type' => 'fieldset', '#title' => t('Basic Setting'));

    $list['message'] = array(
      '#type' => 'text_format',
      '#title' => t('Payment Form Message'),
      '#default_value' => $default['message'],
      '#format' => $default['text_format'],
    );

    $list['batch_number'] = array(
      '#type' => 'select',
      '#title' => t('Batch Number'),
      '#options' => $this->batchNumberOption(),
      '#default_value' => $default['batch_number'],
      '#required' => TRUE,
    );

    $list['prefix_code'] = array(
      '#type' => 'select',
      '#title' => t('Prefix Code'),
      '#options' => $this->prefixCodeOption(),
      '#default_value' => $default['prefix_code'],
      '#required' => TRUE,
    );

    $list['execute_mode'] = array(
      '#type' => 'select',
      '#title' => t('Execute Mode'),
      '#options' => $this->executeModeOption(),
      '#default_value' => $default['execute_mode'],
      '#required' => TRUE,
    );

    $list['merchant_code'] = array(
      '#type' => 'textfield',
      '#title' => t('Merchant Code'),
      '#default_value' => $default['merchant_code'],
      '#maxlength' => $field['merchant_code']['length'],
      '#required' => TRUE,
    );

    $list['merchant_key'] = array(
      '#type' => 'textfield',
      '#title' => t('Merchant Key'),
      '#default_value' => $default['merchant_key'],
      '#maxlength' => $field['merchant_key']['length'],
      '#required' => TRUE,
    );

    return $list;
  }

  function methodSettingCurrencyFieldset(array $data) {
    $default = $data['default'];
    $list = array('#type' => 'fieldset', '#title' => t('Currency Setting'));

    $list['payment_currency'] = array(
      '#type' => 'select',
      '#title' => t('Payment Currency'),
      '#options' => $this->paymentCurrencyOption(),
      '#default_value' => $default['payment_currency'],
      '#required' => TRUE,
    );

    $list['exchange_rate'] = array(
      '#type' => 'textfield',
      '#title' => t('Exchange Rate'),
      '#description' => t('The rate of default currency exchange to payment currency. 0 for get rate online, others for fixed rate.'),
      '#default_value' => $default['exchange_rate'],
      '#required' => TRUE,
    );

    $list['minimum_amount'] = array(
      '#type' => 'textfield',
      '#title' => t('Minimum Amount'),
      '#description' => t('Amount must greater than this value.'),
      '#default_value' => $default['minimum_amount'],
      '#required' => TRUE,
    );

    $list['maximum_amount'] = array(
      '#type' => 'textfield',
      '#title' => t('Maximum Amount'),
      '#description' => t('Amount must less than this value. 0 indicates that the maximum integer (@limit).', array('@limit' => PHP_INT_MAX)),
      '#default_value' => $default['minimum_amount'],
      '#required' => TRUE,
    );

    $list['payment_decimal'] = array(
      '#type' => 'textfield',
      '#title' => t('Payment Decimal'),
      '#default_value' => $default['payment_decimal'],
      '#required' => TRUE,
    );

    $list['display_decimal'] = array(
      '#type' => 'textfield',
      '#title' => t('Display Decimal'),
      '#default_value' => $default['display_decimal'],
      '#required' => TRUE,
    );

    $list['display_currency_code'] = array(
      '#type' => 'select',
      '#title' => t('Display Currency Code'),
      '#options' => $this->displayCurrencyCodeOption(),
      '#default_value' => $default['display_currency_code'],
      '#required' => TRUE,
    );

    return $list;
  }

  function methodSettingValidate(array $element, array &$form_state) {
    $list = PaymentGateway::mergeFormData($element, $form_state);
    $index = 'payment_method';

    if ((FALSE == isset($form_state[$index])) || (FALSE == PaymentGateway::checkMethod($form_state[$index]))) {
      form_error($element, t('Method is not vaild.'));
      return;
    }

    $method = &$form_state[$index];
    $basic = &$list['basic'];
    $message = &$basic['message'];
    $data = $basic + PaymentGateway::controllerData($method);
    $data['text_format'] = $message['format'];
    $data['message'] = $message['value'];

    foreach ($this->mode as $mode) {
      $index = $mode['name'];

      if (FALSE == empty($list[$index])) {
        $data = $list[$index] + $data;
      }
    }

    $method->controller_data = $data;
  }

  function methodSettingModeCallback(array &$data, array $mode) {
    $form = &$data['form'];

    $form[$mode['name']] = array(
      '#type' => 'fieldset',
      '#title' => t('@mode Setting', array('@mode' => $mode['title'])),
    );
  }

  function methodSettingActionCallback(array &$data, array $mode, array $action) {
    if ($action['offline']) {
      retrun;
    }

    $modeKey = $mode['name'];
    $index = PaymentGateway::invokeUrlKey($modeKey, $action['name']);
    $form = &$data['form'];
    $default = $data['default'];
    $field = $data['field'];

    $form[$modeKey][$index] = array(
      '#type' => 'textfield',
      '#title' => t('@action URL', array('@action' => $action['title'])),
      '#default_value' => $default[$index],
      '#maxlength' => $field[$index]['length'],
    );
  }

  // </editor-fold>
  // <editor-fold desc="Payment Setting">
  function paymentSettingDefault() {
    $form = array();
    $form['basic'] = array('#type' => 'fieldset', '#title' => t('Basic'));
    $form['setting'] = array('#type' => 'fieldset', '#title' => t('Setting'));

    $basic = &$form['basic'];
    $basic['error'] = array('#type' => 'markup');
    $basic['message'] = array('#type' => 'markup');
    $basic['total'] = array('#type' => 'item', '#title' => t('Total:'));
    return $form;
  }

  function paymentSetting(array $element, array &$form_state) {
//    dpm($element);
    $form = $this->paymentSettingDefault();
    $basic = &$form['basic'];
    $error = &$basic['error'];
    $errorText = '<span class="format-currency format-currency-error">@message</span>';
    $index = 'payment';

    if ((FALSE == isset($form_state[$index])) || (FALSE == PaymentGateway::checkPayment($form_state[$index]))) {
      $error['#markup'] = check_markup(format_string($errorText, array('@message' => t('Payment is not valid.'))));
      return $form;
    }

    $payment = $form_state[$index];
    $total = PaymentGateway::total($payment);

    if (0 >= $total['rate']) {
      $error['#markup'] = check_markup(format_string($errorText, array('@message' => $total['message'])));
      return $form;
    }

    $data = PaymentGateway::controllerData($payment->method);
    PaymentGateway::setFormatCurrencyOption($total, $data);

    $basic['message']['#markup'] = check_markup($data['message'], $data['text_format']);
    $basic['total']['#markup'] = check_markup(PaymentGateway::formatCurrency($total));
    return $form;
  }

  function paymentSettingValidate(array $element, array &$form_state) {
    $list = &PaymentGateway::mergeFormData($element, $form_state);
    $index = 'payment';

    if ((FALSE == isset($form_state[$index])) || (FALSE == PaymentGateway::checkPayment($form_state[$index]))) {
      form_error($element, t('Payment is not vaild.'));
      return;
    }

    $payment = &$form_state[$index];
    $payment->context_data = $list + $payment->context_data;
  }

  // </editor-fold>

  function modeActionLoop(&$data, $modeCallback, $actionCallback) {
    foreach ($this->mode as $mode) {
      if (is_callable($modeCallback)) {
        call_user_func_array($modeCallback, array(&$data, &$mode));
      }

      foreach ($this->action as $action) {
        if (is_callable($actionCallback)) {
          call_user_func_array($actionCallback, array(&$data, &$mode, &$action));
        }
      }
    }
  }

}

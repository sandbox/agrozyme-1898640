<?php

namespace Drupal\payment_gateway;

abstract class Payment extends Action {

  function before() {
    $payment = $this->payment;
    $controller = $payment->method->controller;
    $this->updateAmount();
    $payment->context_data['number'] = $controller->encodeOrderNumber($payment);
    parent::before();
  }

  function process() {
    return $this->callback();
  }

}

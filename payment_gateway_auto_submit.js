;
(function ($) {
// Automatically submit the payment redirect form.
  Drupal.behaviors.PaymentGateway = {
    attach: function (context, settings) {
      $('form.payment-gateway', context).submit();
    }
  };
})(jQuery);
